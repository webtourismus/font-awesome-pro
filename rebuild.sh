#!/usr/bin/env sh

FA_SVG_BASE_DIR="./vendor/fortawesome/font-awesome-pro/svgs/"

echo ""
echo "Rebuilding custom dist of Font Awesome 5 pro"
echo ""

if [ ! -d "$FA_SVG_BASE_DIR" ]; then
  echo "FEHLER"
  echo ""
  echo "Das Verzeichnis $FA_SVG_BASE_DIR existiert nicht. Starte composer install --require-dev"
  echo ""
  exit 1
fi

if [ ! -d "./light" ]; then
  mkdir light
fi

if [ ! -d "./regular" ]; then
  mkdir regular
fi

if [ ! -d "./solid" ]; then
  mkdir solid
fi

if [ ! -d "./brands" ]; then
  mkdir brands
fi

if [ ! -d "./duotone" ]; then
  mkdir duotone
fi

if [ ! -d "./white" ]; then
  mkdir white
fi

rm -f ./light/*.svg
rm -f ./regular/*.svg
rm -f ./solid/*.svg
rm -f ./brands/*.svg
rm -f ./duotone/*.svg
rm -f ./white/*.svg

for DIR in ${FA_SVG_BASE_DIR}*/;
do
  FA_SVG_STYLE_DIR=${DIR%/}
  STYLE_NAME=${FA_SVG_STYLE_DIR#$FA_SVG_BASE_DIR}
  for FILE in ${DIR}*.svg;
  do
    WIDTH="$(grep -Po 'viewBox=\"0 0\K \d+' $FILE)"
    HEIGHT="$(grep -Po 'viewBox=\"0 0 \d+\K \d+' $FILE)"
    FACTOR=$(php -r "echo ceil($WIDTH / $HEIGHT * 16);")
    FILENAME=${FILE#$DIR}
    ICONNAME=${FILENAME%.svg}
    sed 's/viewBox=/class="svg svg--fa svg--'$STYLE_NAME' fa-'$ICONNAME' fa-w-'$FACTOR'" fill="currentColor" viewBox=/' ${FILE} > ./${STYLE_NAME}/${FILENAME}
  done
done

for FILE in ${FA_SVG_BASE_DIR}/regular/*.svg;
do
  WIDTH="$(grep -Po 'viewBox=\"0 0\K \d+' $FILE)"
  HEIGHT="$(grep -Po 'viewBox=\"0 0 \d+\K \d+' $FILE)"
  FACTOR=$(php -r "echo ceil($WIDTH / $HEIGHT * 16);")
  FILENAME=${FILE#$FA_SVG_BASE_DIR/regular/}
  ICONNAME=${FILENAME%.svg}
  sed 's/viewBox=/class="svg svg--fa svg--'$STYLE_NAME' fa-'$ICONNAME' fa-w-'$FACTOR'" fill="white" viewBox=/' ${FILE} > ./white/${FILENAME}
done
